import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app';

import './styles/styles.scss';

export function main(): Promise<any> {
  return platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .catch((err: Error) => console.error(err));
}

main();
