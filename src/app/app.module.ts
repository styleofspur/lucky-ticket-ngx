import { BrowserModule } from '@angular/platform-browser';
import { RouterModule }  from '@angular/router';
import {
  NgModule
} from '@angular/core';

// Platform and environment providers/directives/pipes
import { ROUTES } from './app.routes'

import { AppComponent }  from './app.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  bootstrap:    [ AppComponent ],
  declarations: [
    AppComponent,

    // pages
    HomeComponent
  ],

  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false, // compatibility for old browsers
    })
  ]
})
export class AppModule {}
