import {
  async,
  TestBed,
  ComponentFixture
} from '@angular/core/testing';

/**
 * Load the implementations that should be tested
 */
import { HomeComponent } from './home.component';

describe('Home', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  /**
   * async beforeEach.
   *
   * The test setup for AppComponent must give the Angular template compiler time to read the files.
   * The body of the async argument looks much like the body of a synchronous beforeEach.
   *
   * There is nothing obviously asynchronous about it.
   * For example, it doesn't return a promise
   * and there is no done function to call
   * as there would be in standard Jasmine asynchronous tests. Internally,
   * async arranges for the body of the beforeEach
   * to run in a special async test zone that hides the mechanics of asynchronous execution.
   *
   * All this is necessary in order to call the asynchronous TestBed.compileComponents method.
   */
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })

    /**
     * Compile template and css.
     *
     * The TestBed.compileComponents method asynchronously compiles all the components
     * configured in the testing module.
     *
     * In this example, the AppComponent is the only component to compile.
     *
     * When compileComponents completes, the external templates and css files
     * have been "inlined"
     * and TestBed.createComponent can create new instances of AppComponent synchronously.
     */
    .compileComponents();
  }));

  /**
   * Synchronous beforeEach.
   *
   * The compileComponents method returns a promise
   * so you can perform additional tasks immediately after it finishes.
   *
   * For example, you could move the synchronous code in the second beforeEach
   * into a compileComponents().then(...) callback and write only one beforeEach.
   *
   * Most developers find that hard to read. The two beforeEach calls are widely preferred.
   */
  beforeEach(() => {
    fixture   = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;

    /**
     * Trigger initial data binding
     */
    fixture.detectChanges();
  });

  it(`should be readily initialized`, () => {
    expect(fixture).toBeDefined();
    expect(component).toBeDefined();
  });

  it(`should have void ngOnInit() method`, () => {
    expect(typeof component.ngOnInit).toBe('function');

    spyOn(component, 'ngOnInit');

    expect(component.ngOnInit()).toBeUndefined();
    expect(component.ngOnInit).toHaveBeenCalled();
    expect(component.ngOnInit).toHaveBeenCalledTimes(1);
  });

  it(`should log ngOnInit() method`, () => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    component.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  });


});
