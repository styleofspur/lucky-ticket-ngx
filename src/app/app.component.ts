import {
  Component,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'lucky-ticket-app',

  /**
   * By default this param has ViewEncapsulation.Emulated value.
   * It means that angular will emulate ShadowDOM behaviour.
   *
   * ShadowDOM means that every component has a CSS context which is hidden from the outside world
   * like if you create a var in a function, its only visible to that function.
   * Angular 2 emulates that by settings special, random, unique attributes for every element
   * that interacts with CSS which is in the styles or styleUrls component meta property.
   *
   * But for e2e tests we need classical CSS selectors to do any assertions, so we don't need ShadowDOM here.
   *
   * See: http://blog.assaf.co/angular-2-webpack-sass-and-viewencapsulation/
   */
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css'
  ],
  templateUrl: './app.component.html'
})
export class AppComponent {

}