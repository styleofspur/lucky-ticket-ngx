import { browser, element, by } from 'protractor';

describe('App', () => {

  beforeEach(async () => {
    await browser.get('/');
  });

  it('should have a title', async () => {
    let subject = await browser.getTitle();
    let result  = 'Lucky Ticket';

    expect(subject).toEqual(result);
  });

  it('should have root element', async () => {
    let subject = await element(by.className('lucky-ticket-app')).isPresent();
    let result  = true;

    expect(subject).toEqual(result);
  });

  it('should be 100% height', async () => {
    let subject = await element(by.className('lucky-ticket-app')).getCssValue('height');
    let result  = await element(by.tagName('body')).getCssValue('height');

    expect(subject).toEqual(result);
  });

});