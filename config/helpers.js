import path from 'path';

const ROOT = path.resolve(__dirname, '..');

/**
 * For any invocation we have path as context and ROOT as first argument.
 */
export const root = path.join.bind(path, ROOT);
